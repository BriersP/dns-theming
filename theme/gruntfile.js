module.exports = function(grunt) {

  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      local: {
        options: {
          sourcemap: true, // GEM UPDATE SASS!
          unixNewlines: true,
          style: 'compressed',
          lineNumbers: true
        },
        files: {
          'css/custom.css': 'sass/custom.scss',
          'css/styleguide.css' : 'sass/styleguide.scss'
        }
      }
    },
    imagemin: {
      local: {
        options: {
          pngquant: true
        },
        files: [{
            expand: true,
            cwd: 'images/',
            src: ['**/*.{png,jpg,gif}'],
            dest: 'images/'
          }]
      }
    },
//    jshint: {
//      all: ['gruntfile.js', 'js/*.js']
//    },
    autoprefixer: {
      local: {
        options: {
          // Task-specific options go here.
          // @see: https://github.com/ai/autoprefixer#browsers
          browsers: ['last 5 version', 'ie 8', 'ie 9'],
          map: true
        },
        src: 'css/*.css'
      }
    },
    watch: {
      styles: {
        files: ['sass/**/*.*'],
        tasks: ['sass', 'autoprefixer']
      },
      images: {
        files: ['images/**/*.*'],
        tasks: ['imagemin']
      },
      options: {
        livereload: true
      }
    }
  });

  grunt.registerTask('default', ['sass', 'imagemin', 'autoprefixer']);
  grunt.registerTask('style', ['sass', 'autoprefixer']);

};

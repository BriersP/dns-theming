(function($) {
  Drupal.behaviors.theming = {
    attach: function(context, settings) {

      // Less padding when hiding page title.
      if ($('.node.hidepagetitle').length || !$('.field-name-field-referenced-icon').length) {
        $('.panel-dns-1col').addClass('hidepagetitle');
      }

      // Angular needs the anchor stuff to be persisted when switching language.
      $('ul.language-switcher-locale-url li a').bind("click", function(e) {
        e.preventDefault();
        var anchor = window.location.hash.substring(1);
        document.location = $(this).attr("href") + "#" + anchor;
      });
      $('form').find('input.error').each(function() {
        $(this).parent().find('label').addClass('error');
      });

      // FAQ
      $('.view-faq .field-name-title h2, .view-id-search .node-faq .field-name-title h2').click(function() {
        // Classes
        $('.view-faq .field-name-title h2, view-id-search .node-faq .field-name-title h2').not(this).removeClass('active');
        $(this).toggleClass('active');

        // Hide and show
        $('.view-faq .group-answer, view-id-search .node-faq .group-answer ').not($(this).parents('.node-faq').find('.group-answer')).hide();
        $(this).parents('.node-faq').find('.group-answer').toggle().toggleClass('active');
      });

      // Language choser, check for cookie.
      if ($('body').hasClass('front')) {
        var language_chosen = $.cookie('language');
        if (typeof language_chosen === "undefined") {
          $('#block-dns-dns-blocks-language-choose').show();
        }
      }


      if ($('body.front').length) {
        $('.banner_clickable').click(function() {
          window.location = $('.hideme').attr('href');
        });
      }

      if ($('body.page-faq').length) {
        if (window.location.hash) {
          var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
          $('.view-id-faq .view-content .views-row > .' + hash + ' > .field-name-title h2').click();
        }
      }

      if ($('.panel-dns-2col').length) {
        $('#content').addClass('twocolumns');
      }

      // page search reset button
      $('.page-search .pane-dns-dns-blocks-reset-button a').appendTo('.page-search .panel-col-last .pane-views-exp-search-panel-pane-1 .views-exposed-widgets').addClass('resetlink');

      // hight of content footer
      $('.region-content .panel-col-content > .inside').wrapAll('<div class="leftside">');
      var height_total = $('.region-content .panel-dns-2col').height();
      var height_content = $('.region-content .panel-col-content > .leftside > .content').height();
      $('.region-content .panel-col-content > .leftside > .footer').height(height_total - height_content + 1);

      // main menu to mobile
      if (!$('#menubox-top .mobile-menu-button').length) {
        $('<div class="mobile-menu-button"><div class="inner">Menu</div></div>').insertBefore('#menubox-top .block-menu-block');
      }
      $('#menubox-top .mobile-menu-button').click(function() {
        $(this).toggleClass('open');
        $(this).next().slideToggle(200);
      });
      $('#menubox-top .block-menu-block a').wrap('<div class="mobilelinkwrapper">');
      $('#menubox-top .mobilelinkwrapper').click(function() {
        $(this).parent().siblings().hide();
        $(this).toggleClass('open');
        $(this).next().slideToggle(200);
        if (!$(this).prev('.back-to-full-menu').length) {
          $('<div class="back-to-full-menu"></div>').insertBefore($(this));
        }
        $('.back-to-full-menu').click(function() {
          $(this).parent().siblings().show();
          $(this).next().removeClass('open');
          $(this).next().next().hide();
          $(this).remove();
        });
      });

      // Second level.

      if ($('body').find('#second_level').length) {
        if (!$('#second_level_parent').length) {
          $('#second_level_inner .item-list').addClass('more_padding');
        }
      }

      // Sticky nav
      $('#menubox-top').appendTo('#header-bottom');

      // mobile searchbox and topmenu to header-bottom
      $('#block-views-exp-search-panel-pane-1').clone().insertBefore('#header-bottom #menubox-top');
      if (!$('.mobile-search-button').length) {
        $('<div class="mobile-search-button"></div>').insertBefore('#header-bottom #block-views-exp-search-panel-pane-1');
      }
      $('.mobile-search-button').click(function() {
        $(this).toggleClass('open');
        $(this).next().slideToggle(200);
      });

      // mobile top menu Contact
      $('#header-top #block-menu-menu-header-menu ul.menu').clone().insertBefore('#header-bottom .mobile-search-button').addClass('mobile-contact-menu');
      $('#header-top .block-locale ul.language-switcher-locale-url').clone().appendTo('#header-bottom #menubox-top .block-menu-block').wrap('<div id="mob-lang">');
      // glitch language
      $('#block-dns-dns-blocks-language-choose').prependTo('body');
    }
  };
  Drupal.behaviors.custom_form_fields = {
    attach: function(context, settings) {
      $('.cff-file').each(function(i, el) {
        $(el).cffFile();
      });
      $('.cff-select').each(function(i, el) {
        $(el).cffSelect();
      });
      $('input').click(function() { // IE8 compatibility for checkboxes and radio
        $('input:not(:checked)').removeClass("checked");
        $('input:checked').addClass("checked");
      });
//      $('input.error').after('<div class="notice"><span>This is a required field</span></div>')
    }
  };
  Drupal.behaviors.qtip = {
    attach: function(context, settings) {
      // LABEL COLOR ON ERROR
      $('.qtip-link').each(function() {
        $(this).qtip({
          content: {
            text: $(this).children('.qtip-tooltip'),
            title: {
              button: true
            }
          },
          show: {
            event: 'click',
            solo: true
          },
          hide: {event: 'unfocus'}
        });
      });
    },
    hide: function(context) {
    }
  };
  Drupal.behaviors.angularjs = {
    attach: function(context, settings) {
      if (typeof Drupal.settings.angularjs != 'undefined' && Drupal.settings.angularjs.base_page) {
        var urlnl = Drupal.behaviors.angularjs.buildUrl('lang', 'nl');
        var urlen = Drupal.behaviors.angularjs.buildUrl('lang', 'en');
        var urlfr = Drupal.behaviors.angularjs.buildUrl('lang', 'fr');
        $('.language-switcher-locale-url .nl a').attr('href', urlnl);
        $('.language-switcher-locale-url .fr a').attr('href', urlfr);
        $('.language-switcher-locale-url .en a').attr('href', urlen);
      }
    },
    buildUrl: function(key, value) {
      base = Drupal.behaviors.angularjs.getHref();
      urlvars = Drupal.behaviors.angularjs.getUrlVars();
      urlvars[key] = value;
      var params = $.param(urlvars);
      return base + '?' + params;
    },
    getHref: function() {
      return window.location.protocol + "//" + window.location.hostname + window.location.pathname;
    },
    getUrlVars: function() {
      var vars = {};
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for (var i = 0; i < hashes.length; i++)
      {
        hash = hashes[i].split('=');
        vars[hash[0]] = hash[1];
      }
      return vars;
    }
  };

})(jQuery);

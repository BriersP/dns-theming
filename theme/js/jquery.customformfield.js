/**
 * Custom Form Field Implementations
 * Simplified Drupalized version, based on Filament Group's customfileinput plugin.
 * @see: https://github.com/filamentgroup/jQuery-Custom-File-Input
 *
 * @author Peter Briers
 * @version 0.15
 */

(function( $ ) {

  $.fn.cffFile = function() {
    var $container = $(this);
    var $button = $('.cff-button', $container);
    var $feedback = $('.cff-feedback', $container);
    var $input = $('.cff-input', $container);
    $container
      .click(function(event) {
        $input.trigger('click');
      });
    $input
      .bind('change',function() {
        var filename = $(this).val().split(/\\/).pop();
        $feedback.text(filename);
      })
      .click(function(event) { //for IE and Opera, make sure change fires after choosing a file, using an async callback
        $input.data('val', $input.val());
        setTimeout(function(){ $input.trigger('checkChange'); }, 100);
        event.stopPropagation();
      })
      .bind('checkChange', function(){
        if($input.val() && $input.val() != $input.data('val')){
          $input.trigger('change');
        }
      });
  }

  $.fn.cffSelect = function() {
    var $container = $(this);
    var $button = $('.cff-button', $container);
    var $feedback = $('.cff-feedback', $container);
    var $input = $('.cff-input', $container);
    var select_text = function() {
      var currentSelected = $container.find(':selected');
      var html = currentSelected.html() || '&nbsp;';
      $feedback.html(html);
    }
    $input
      .on('change', function(){
        select_text();
      });
      select_text();
  }
})( jQuery );

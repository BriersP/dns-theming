(function ($) {
    Drupal.behaviors.stickyHeader = {
        attach: function (context, settings) {
            if ($('#header-bottom').length) {
                var scroll, wresize, mobile;
                var headerPos = $('#header-bottom').offset().top;
                var once = true;
                var init = false;
                var show, go;
                (scroll = function () {
                    if ($('#header-bottom').css('position') != 'fixed') {
                        if ($('#header-bottom').hasClass('mobileTop') || $(window).width() < 740) {
                            headerPos = 7;
                        }
                        else {
                            headerPos = 37;
                        }
                        if ($('body > #sliding-popup').length) {
                            var scrollPos = $(document).scrollTop() - 140;
                        } else {
                            var scrollPos = $(document).scrollTop();
                        }
                        if (scrollPos > headerPos) {
                            clearTimeout(show);
                            init = true;
                            $('#header-bottom').addClass('attached').css({'top': (scrollPos - headerPos) + 'px'});

                        } else if (init === true) {
                            clearTimeout(go);
                            $('#header-bottom').removeClass('attached').css({'top': '0px'});
                            once = true;
                            init = false;
                        }
                    }

                })();

                (wresize = function () {
                    var windowwidth = $(window).width();
                    if (windowwidth < 740) {
                        $('#header-bottom').addClass('mobileTop');
                    }
                    else {
                        $('#header-bottom').removeClass('mobileTop');
                    }
                });

                $(document).scroll(scroll);
                $(window).resize(wresize);
            }
        }
    };

})(jQuery);

<?php

function dns_theme_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  /**
   * If taxonomy page add voc name to breadcrumb.
   */
  $path = request_path();
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $tid = arg(2);
    if ($term = taxonomy_term_load($tid)) {
      $voc = taxonomy_vocabulary_load($term->vid);
      $breadcrumb[] = '<span>' . $voc->name . "</span>";
    }
  }
  else if (stripos($path, "faq") && isset($_GET['f'])) {
    $current = $breadcrumb[sizeof($breadcrumb) -1];
    $breadcrumb[sizeof($breadcrumb) -1] = l(t('FAQ'), 'faq');
    $breadcrumb[] = $current;
  }
  else if (strtolower(arg(0)) == 'news') {
    if (isset($_GET['f'])) {
      $breadcrumb[2] = $breadcrumb[1];
      $breadcrumb[1] = l(t('News & Blogs'), 'news');
    }
    else {
      $breadcrumb[1] = t('News & Blogs');
    }
  }
  else {
    $breadcrumb[] = drupal_get_title();
  }



  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $breadcrumb[sizeof($breadcrumb) - 1] = '<span class="current_breadcrumb">' . end($breadcrumb) . '</span>';
    $output .= '<div class="breadcrumb">' . implode(' <span class="beforetitle">></span> ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Set second menu level if available.
 *
 * @param type $vars
 */
function dns_theme_preprocess_page(&$vars) {
  $active_trail = menu_get_active_trail();
  if (sizeof($active_trail) > 1) {
    $active_item = end($active_trail);
    if (isset($active_item['menu_name'])) {
      $children = dns_get_menu_item_children($active_item['link_path']);
      if ($active_item['depth'] == 2) {
        if (!empty($children)) {
          $active = l($active_item['link_title'], $active_item['link_path']);
          $vars['second_level_parent'] = $active;
        }
        else {
          $parent = _menu_link_find_parent($active_item);
          $active = l($parent['link_title'], $parent['link_path']);
          $vars['second_level_parent'] = $active;
          $children = dns_get_menu_item_children($parent['link_path']);
        }
      }
      if ($active_item['depth'] == 3) {
        $parent = _menu_link_find_parent($active_item);
        $active = l($parent['link_title'], $parent['link_path']);
        $vars['second_level_parent'] = $active;
        $children = dns_get_menu_item_children($parent['link_path']);
      }
      $items = array();
      if (!empty($children)) {
        foreach ($children as $key => $child) {
          if (!$child['link']['hidden'] == "1") {
            $items[] = l($child['link']['link_title'], $child['link']['link_path']);
          }
        }
        $vars['second_level'] = theme('item_list', array('items' => $items));
      }
    }
  }
  $hide_title = array('news');
  if (in_array(substr(request_path(), 3), $hide_title)) {
    $vars['title'] = "";
  }

  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie8.css', array(
    'group' => CSS_THEME,
    'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE),
    'preprocess' => FALSE,
    'weight' => 50
  ));
}

function dns_theme_file($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'file';
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-file', 'cff-input'));

  $html = '<div class="cff-container cff-file">';
  $html .= '<input' . drupal_attributes($element['#attributes']) . ' />';
  $html .= '<div class="cff-replacement">';
  $html .= '<span class="cff-button">' . t('Choose file') . '</span>';
  $html .= '<span class="cff-feedback">' . t('No file selected...') . '</span>';
  $html .= '</div>';
  $html .= '</div>';

  return $html;
}

function dns_theme_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select', 'cff-input'));

  $html = '<div class="cff-container cff-select">';
  $html .= '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>';
  $html .= '<div class="cff-replacement">';
  $html .= '<span class="cff-button"></span>';
  $html .= '<span class="cff-feedback"></span>';
  $html .= '</div>';
  $html .= '</div>';

  return $html;
}

function dns_theme_preprocess_node(&$variables) {
  if ($variables['type'] == "faq") {
    if ($variables['view_mode'] == 'full') {
      $variables['classes_array'][] = 'faq-open-' . $variables['nid'];
    }
  }
  if ($variables['type'] == "blok") {
    $node = $variables['node'];
    if ($variables['view_mode'] == 'full') {
      if (isset($node->field_block_hide_title)) {
        $hide = field_get_items('node', $node, 'field_block_hide_title');
        if ($hide[0]['value'] == 1) {
          $variables['content']['title'][0]['#markup'] = "";
        }
      }
    }
  }

  if ($variables['type'] == "page") {
    $node = $variables['node'];
    if ($variables['view_mode'] == 'full') {
      if (isset($node->field_page_hide_page_title)) {
        $hide = field_get_items('node', $node, 'field_page_hide_page_title');
        if ($hide[0]['value'] == 1) {
          $variables['classes_array'][] = "hidepagetitle";
          unset($variables['content']['title']);
        }
      }
    }
  }
}

/**
 * Prepares the noscript tag which is used when JavaScript is not available.
 *
 * @param $variables
 *   An array containing a "disqus" array, containing the following items:
 *     - "domain": The domain associated with this Disqus account.
 *     - "title": The title of the thread.
 *     - "developer": Whether or not testing is enabled.
 *     - "url": The disqus_url variable (http://disqus.com/docs/help/#faq-16).
 */
function dns_theme_disqus_noscript($variables = array()) {
  $disqus = $variables['disqus'];
  // Return the comment markup.
  return '<noscript><p>' . t('You need to enable javascript to see the discussions.') . '</p><p>' . l(t('View the discussion thread.'), 'http://' . $disqus['domain'] . '.disqus.com/?url=' . urlencode($disqus['url'])) . '</p></noscript>';
}

/**
 * Theme the outer form. This is required for the fieldset(s) to validate.
 */
function dns_theme_collapse_text_form($element) {
  foreach ($element['element']['collapse_text_internal_text'] as &$item) {
    if (isset($item['#title'])) {
      $item['#title'] = string_sanitize($item['#title']);
    }
  }
  return drupal_render_children($element['element']);
}

function string_sanitize($s) {
  $result = str_replace('"', "", html_entity_decode($s, ENT_QUOTES));
  return $result;
}
